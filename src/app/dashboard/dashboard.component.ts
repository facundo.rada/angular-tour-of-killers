import { Component, OnInit } from '@angular/core';
import { Killer } from '../killer';
import { KillerService } from '../killer.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  killers: Killer[] = [];

  constructor(private killerService: KillerService) { }

  ngOnInit() {
    this.getKillers();
  }

  getKillers(): void {
    this.killerService.getKillers()
      .subscribe(killers => this.killers = killers.slice(1, 5));
  }
}