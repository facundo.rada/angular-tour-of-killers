import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { KillersComponent } from './killers/killers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { KillerDetailComponent } from './killer-detail/killer-detail.component';

const routes: Routes = [
    { path: 'killers', component: KillersComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'detail/:id', component: KillerDetailComponent },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
