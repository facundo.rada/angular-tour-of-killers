import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  public messages: string[] = [];

  constructor() { }

  getMessages(): string[] {
    return this.messages;
  }

  add(message: string): void {
    this.messages.push(message);
  }

  clear(): void {
    this.messages = [];
  }

}
