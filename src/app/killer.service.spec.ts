import { TestBed, inject } from '@angular/core/testing';

import { KillerService } from './killer.service';

describe('KillerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KillerService]
    });
  });

  it('should be created', inject([KillerService], (service: KillerService) => {
    expect(service).toBeTruthy();
  }));
});
