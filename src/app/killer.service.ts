import { Killer } from './killer'
import { KILLERS } from './mock-killers'
import { Injectable } from '@angular/core/';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';

@Injectable()
export class KillerService {

  constructor(private messagesService: MessageService) { }

  getKillers(): Observable<Killer[]> {
    // Todo: send the message _after_ fetching the heroe
    this.messagesService.add("KillerService: fetched killers");
    return of(KILLERS);
  }

  getKillerById(id: number): Observable<Killer> {
    // Todo: send the message _after_ fetching the heroe
    this.messagesService.add(`KillerService: fetched killer by id: ${id}`);
    return of(KILLERS.find(killer => killer.id == id));
  }

}
