import { Killer } from "./killer";

export const KILLERS: Killer[] = [
    { id: 11, name: 'Ted Bundy' },
    { id: 12, name: 'The Zodiac Killer' },
    { id: 13, name: 'Donald Henry Gaskins' },
    { id: 14, name: 'Tsutomu Miyazaki' },
    { id: 15, name: 'Jack the Ripper' },
    { id: 16, name: 'Luis Garavito' },
    { id: 17, name: 'Ahmad Suradji' },
    { id: 18, name: 'Alexander Pichushkin' },
    { id: 19, name: 'Andrei Chikatilo' },
    { id: 20, name: 'Charles Edmund Cullen' },
    { id: 21, name: 'Patrick Wayne Kearney' },
    { id: 22, name: 'Dennis Raider' },
    { id: 23, name: 'John George Haigh' },
    { id: 24, name: 'Paul Knowles' },
    { id: 25, name: 'William Bonin' },
    { id: 26, name: 'Aileen Wuornos' },
    { id: 27, name: 'Jeffrey Dahmer' },
    { id: 28, name: 'John Wayne Gacy' },
    { id: 29, name: 'Tommy Lynn Sells' },
    { id: 30, name: 'Pedro Rodriguez Filho' }
];