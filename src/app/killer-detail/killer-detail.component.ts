import { Component, OnInit, Input } from '@angular/core';
import { Killer } from '../killer';
import { KillerService } from '../killer.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-killer-detail',
  templateUrl: './killer-detail.component.html',
  styleUrls: ['./killer-detail.component.css']
})
export class KillerDetailComponent implements OnInit {
  killer: Killer;
  constructor(
    private killerService: KillerService,
    private activatedRoute: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    this.getKiller();
  }

  getKiller(): void {
    let id: number = NaN;
    this.activatedRoute.params.subscribe(params => {
        id = params["id"];
    });
    //oher way to get a param from url
    const killerId = +this.activatedRoute.snapshot.paramMap.get('id');
    this.killerService.getKillerById(killerId)
      .subscribe((killer) => { this.killer = killer; });
  }

  goBack(): void {
    this.location.back();
  }

}
