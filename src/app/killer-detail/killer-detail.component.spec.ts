import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KillerDetailComponent } from './killer-detail.component';

describe('KillerDetailComponent', () => {
  let component: KillerDetailComponent;
  let fixture: ComponentFixture<KillerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KillerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KillerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
