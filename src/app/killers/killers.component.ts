import { Component, OnInit } from '@angular/core';
import { Killer } from "../killer";
import { KillerService } from '../killer.service';

@Component({
  selector: 'app-killers',
  templateUrl: './killers.component.html',
  styleUrls: ['./killers.component.css']
})
export class KillersComponent implements OnInit {
  killers:Killer[] = [];

  constructor(private killerService: KillerService) { }

  ngOnInit() {
    this.getKillers();
  }

  getKillers(): void {
     this.killerService.getKillers().subscribe(killers => this.killers = killers);
  }

}
